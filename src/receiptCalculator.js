class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (parameter) {
    if (parameter.length === 0) {
      return 0;
    }
    let find = false;
    let totalPrice = 0;

    for (let i = 0; i < parameter.length; i++) {
      for (let j = 0; j < this.products.length; j++) {
        const temp = this.products[j];
        if (temp.id === parameter[i]) {
          find = true;
          totalPrice += temp.price;
        }
      }
    }
    if (find === true) {
      return totalPrice;
    } else {
      throw new Error('Product not found');
    }
  }
  // --end->
}

export default RecepitCalculator;
